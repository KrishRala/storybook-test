import React, { useState } from "react";
import { useTodosDispatch } from "../contexts/TodosContext";
import { Textfield } from "../components/TextInput";
import { Form, Button } from "antd";

function TodoForm() {
  const [value, setValue] = useState("");
  const dispatch = useTodosDispatch();

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    dispatch({
      type: "CREATE",
      text: value,
    });
    setValue("");
  };

  return (
    <div className="justify-center">
      <Form>
        <Textfield
          value={value}
          placeholder="planning to do?"
          onChange={(e) => setValue(e.target.value)}
        />
        <Button type="primary" onClick={onSubmit}>
          Submit
        </Button>
      </Form>
    </div>
  );
}

export default TodoForm;
