import React from "react";
import TodoItem from "./TodoItem";
import { useTodosState } from "../contexts/TodosContext";

function TodoList() {
  const todos = useTodosState();
  return (
    <div>
      {todos.map((todo) => (
        <TodoItem todo={todo} key={todo.id} />
      ))}
    </div>
  );
}

export default TodoList;
