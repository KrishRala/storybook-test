import React from "react";
import { useTodosDispatch, Todo } from "../contexts/TodosContext";

import { TodoListButton } from "../components/Button/Button";
import { Selector } from "../components/Selector";
import { Card } from "antd";

type TodoItemProps = {
  todo: Todo;
};

function TodoItem({ todo }: TodoItemProps) {
  const dispatch = useTodosDispatch();

  const onToggle = () => {
    dispatch({
      type: "TOGGLE",
      id: todo.id,
    });
  };

  const onRemove = () => {
    dispatch({
      type: "REMOVE",
      id: todo.id,
    });
  };

  return (
    <Card>
      <tr>
        <td>
          <Selector onChange={onToggle} checked={todo.done} />
        </td>
        <td>
          <p>{todo.text}</p>
        </td>
        <td>
          <TodoListButton
            type="primary"
            onClick={onRemove}
            children="remove"
            size="small"
            danger={true}
          />
        </td>
      </tr>
    </Card>
  );
}

export default TodoItem;
