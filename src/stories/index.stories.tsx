import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import TodoForm from "../containers/TodoForm";
import TodoList from "../containers/TodoList";
import { TodosContextProvider } from "../contexts/TodosContext";
import "../scss/tailwind.scss";

export default {
  title: "Home",
};

export const todoList = () => {
  return (
    <TodosContextProvider>
      <TodoForm />
      <TodoList />
    </TodosContextProvider>
  );
};
