import React from "react";
import TodoForm from "./containers/TodoForm";
import TodoList from "./containers/TodoList";
import { TodosContextProvider } from "./contexts/TodosContext";
import { Layout } from "antd";
import "./scss/tailwind.scss";

const { Content } = Layout;

const App = () => {
  return (
    <TodosContextProvider>
      <Layout>
        <Content>
          <TodoForm />
          <TodoList />
        </Content>
      </Layout>
    </TodosContextProvider>
  );
};

export default App;
