import React from "react";
import { Checkbox } from "antd";

export interface SelectProps {
  checked?: boolean;
  onChange?: () => void;
}

export const Selector: React.FC<SelectProps> = ({ onChange, checked }) => {
  return <Checkbox checked={checked} onChange={onChange} />;
};
