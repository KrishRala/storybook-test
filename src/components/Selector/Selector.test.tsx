import { render } from "@testing-library/react";
import React from "react";
import { Selector, SelectProps } from "./Selector";

const mockedOnChange = jest.fn();

const props: SelectProps = {
  onChange: mockedOnChange,
  checked: false,
};

describe("<Select />", () => {
  test("should render the Select component", () => {
    const wrapper = render(<Selector {...props} />);
    expect(wrapper).toBeDefined();
  });
});
