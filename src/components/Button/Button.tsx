import React from "react";
import { Button } from "antd";

export interface ButtonProps {
  children?: string;
  lachildrenble?: string;
  type: "primary" | "default" | "ghost" | "text";
  onClick?: () => void;
  disabled?: boolean;
  danger?: boolean;
  size?: "large" | "middle" | "small";
}

export const TodoListButton: React.FC<ButtonProps> = ({
  children,
  onClick,
  size,
  danger,
  type,
  disabled,
}) => (
  <Button
    size={size}
    onClick={onClick}
    type={type}
    danger={danger}
    disabled={disabled}
  >
    {children}
  </Button>
);
