import { render } from "@testing-library/react";
import React from "react";
import { TodoListButton, ButtonProps } from "./Button";

const props: ButtonProps = {
  children: "button text",
  type: "primary",
};

describe("<Button />", () => {
  test("should render the Button component", () => {
    const wrapper = render(<TodoListButton {...props} />);
    expect(wrapper).toBeDefined();
  });
});
