import React, { ChangeEvent } from "react";
import { Input } from "antd";

export interface TextfieldProps {
  placeholder?: string;
  onChange?: (arg: ChangeEvent<HTMLInputElement>) => void;
  value?: string;
}

export const Textfield: React.FC<TextfieldProps> = ({
  placeholder,
  onChange,
  value,
}) => <Input placeholder={placeholder} onChange={onChange} value={value} />;
