import { render } from "@testing-library/react";
import React from "react";
import { Textfield } from "./Textfield";

describe("<Textfield />", () => {
  test("should render the Textfield component", () => {
    const wrapper = render(<Textfield />);
    expect(wrapper).toBeDefined();
  });
});
